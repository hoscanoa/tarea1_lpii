package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Union;
import conexion.MySqlDBConexion;

/**
 * Servlet implementation class ListaEventoServlet
 */
@WebServlet("/ListaEventoServlet")
public class ListaEventoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Union> lista = new ArrayList<Union>();

		try {
			Connection conn = MySqlDBConexion.getConexion();
			String sql = "select E.nombre, L.nombre, E.fechaInicio, E.fechaFin, E.Capacidad, E.url, E.emailContacto from  eventos.evento E inner join  eventos.local L on E.idLocal=L.idLocal order by E.fechaInicio";
			PreparedStatement pst = conn.prepareStatement(sql);

			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				lista.add(new Union(rs.getString(1), rs.getString(2), rs
						.getString(3), rs.getString(4), rs.getInt(5), rs
						.getString(6), rs.getString(7)));
			}

			request.setAttribute("lista", lista);

		} catch (Exception e) {
			// TODO: handle exception
		}

		request.getRequestDispatcher("lista.jsp").forward(request, response);
	}

}
