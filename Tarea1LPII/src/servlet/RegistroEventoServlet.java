package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conexion.MySqlDBConexion;

/**
 * Servlet implementation class RegistroEventoServlet
 */
@WebServlet("/RegistroEventoServlet")
public class RegistroEventoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			int idLocal = Integer
					.parseInt(request.getParameter("inputIdLocal"));
			String nombre = request.getParameter("inputNombre");
			String fechaInicio = request.getParameter("inputFechaInicio");
			String fechaFin = request.getParameter("inputFechaFin");
			int capacidad = Integer.parseInt(request
					.getParameter("inputCapacidad"));
			String url = request.getParameter("inputUrl");
			String email = request.getParameter("inputEmail");

			System.out.println("" + idLocal);
			System.out.println(nombre);
			System.out.println(fechaInicio);
			System.out.println(fechaFin);
			System.out.println("" + capacidad);
			System.out.println(url);
			System.out.println(email);

			Connection conn = MySqlDBConexion.getConexion();
			String sql = "INSERT INTO evento values(null,?,?,?,?,?,?,?)";
			PreparedStatement pst = conn.prepareStatement(sql);

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date dFechaInicio = sdf.parse(fechaInicio);
			Date dFechaFin = sdf.parse(fechaFin);

			sdf = new SimpleDateFormat("yyyy-MM-dd");

			pst.setInt(1, idLocal);
			pst.setString(2, nombre);
			pst.setString(3, sdf.format(dFechaInicio));
			pst.setString(4, sdf.format(dFechaFin));
			pst.setInt(5, capacidad);
			pst.setString(6, url);
			pst.setString(7, email);
			pst.executeUpdate();
			
			request.setAttribute("exito", true);

			

		} catch (Exception e) {
			request.getRequestDispatcher("error.jsp")
					.forward(request, response);
		}
		
		request.getRequestDispatcher("/ListaEventoServlet").forward(
				request, response);
	}
}
