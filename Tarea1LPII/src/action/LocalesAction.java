package action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import conexion.MySqlDBConexion;
import bean.Local;

public class LocalesAction {
	public ArrayList<Local> listarLocales() {
		ArrayList<Local> locales = new ArrayList<Local>();

		Connection cn = MySqlDBConexion.getConexion();
		try {
			String sql = "SELECT * FROM local";
			PreparedStatement pst = cn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();

			while (rs.next()) {
				locales.add(new Local(rs.getInt(1), rs.getString(2)));
			}

		} catch (Exception e) {

		}
		return locales;
	}
}
