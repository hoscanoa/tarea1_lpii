package bean;

public class Union {
	private String nombreEvento;
	private String nombreLocal;	
	private String fechaInicio;
	private String fechaFin;
	private int capacidad;
	private String url;
	private String email;
	public String getNombreLocal() {
		return nombreLocal;
	}
	public void setNombreLocal(String nombreLocal) {
		this.nombreLocal = nombreLocal;
	}
	public String getNombreEvento() {
		return nombreEvento;
	}
	public void setNombreEvento(String nombreEvento) {
		this.nombreEvento = nombreEvento;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public int getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Union(String nombreEvento, String nombreLocal, String fechaInicio,
			String fechaFin, int capacidad, String url, String email) {
		this.nombreEvento = nombreEvento;
		this.nombreLocal = nombreLocal;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.capacidad = capacidad;
		this.url = url;
		this.email = email;
	}
	public Union() {

	}

}
