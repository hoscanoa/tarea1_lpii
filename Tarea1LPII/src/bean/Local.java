package bean;

public class Local {
	private int idLocal;
	private String nombre;
	public int getIdLocal() {
		return idLocal;
	}
	public void setIdLocal(int idLocal) {
		this.idLocal = idLocal;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Local(int idLocal, String nombre) {
		this.idLocal = idLocal;
		this.nombre = nombre;
	}

	public Local() {

	}
}
