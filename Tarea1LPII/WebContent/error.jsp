<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

<title>Error</title>
</head>
<body>
	<div class="container">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h3 class="panel-title">Error</h3>
			</div>
			<div class="panel-body">No se pudo registrar el evento</div>
		</div>

		<ul class="pager">
			<li class="previous"><a href="index.jsp">← Regresar</a></li>
		</ul>
	</div>
</body>
</html>