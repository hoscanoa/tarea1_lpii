<%@page import="action.LocalesAction"%>
<%@page import="java.util.ArrayList"%>
<%@page import="bean.Local"%>
<%@page import="bean.Evento"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.metadata.js"></script>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

<script type="text/javascript">
	$(document).ready(function() {
		$("#dataEvento").validate();
	});
</script>


<title>Registro de Eventos Tecnológicos</title>
</head>
<body>

	<div class="container">

		<div class="well bs-component">
			<form class="form-horizontal" id="dataEvento"
				action="RegistroEventoServlet">
				<fieldset>
					<legend>Registro de Eventos Tecnológicos</legend>

					<div class="form-group">
						<label for="inputNombre" class="col-lg-2 control-label">Nombre
							del Evento</label>
						<div class="col-lg-10">
							<input type="text"
								class="form-control {required:true,rangelength:[5,100]}"
								id="inputNombre" name="inputNombre"
								placeholder="Ingrese el nombre del evento" />
						</div>
					</div>
					
					<div class="form-group">
						<label for="select" class="col-lg-2 control-label">Local</label>
						<div class="col-lg-10">
							<select class="form-control" id="select" name="inputIdLocal">
								<%
									ArrayList<Local> locales = new LocalesAction().listarLocales();
									for (int i = 0; i < locales.size(); i++) {
								%>
								<option value=<%=locales.get(i).getIdLocal()%>><%=locales.get(i).getNombre()%>
								</option>
								<%
									}
								%>
							</select> <br />
						</div>
					</div>

					<div class="form-group">
						<label for="inputFechaInicio" class="col-lg-2 control-label">Fecha
							de Inicio</label>
						<div class="col-lg-10">
							<input type="text"
								class="form-control {required:true,dateISO:true}"
								id="inputFechaInicio" name="inputFechaInicio"
								placeholder="dd-mm-aaaa" />
						</div>
					</div>


					<div class="form-group">
						<label for="inputUrl" class="col-lg-2 control-label">Fecha
							de Fin</label>
						<div class="col-lg-10">
							<input type="text"
								class="form-control {required:true,dateISO:true}"
								id="inputFechaFin" name="inputFechaFin" placeholder="dd-mm-aaaa" />
						</div>
					</div>

					<div class="form-group">
						<label for="inputCapacidad" class="col-lg-2 control-label">Capacidad</label>
						<div class="col-lg-10">
							<input type="text"
								class="form-control {required:true,digits:true,range:[1,199]}"
								id="inputCapacidad" name="inputCapacidad"
								placeholder="Un valor entre 0 y 200" />
						</div>
					</div>

					<div class="form-group">
						<label for="inputUrl" class="col-lg-2 control-label">Url</label>
						<div class="col-lg-10">
							<input type="text" class="form-control {required:true,url:true}"
								id="inputUrl" name="inputUrl" placeholder="Url del evento" />
						</div>
					</div>

					<div class="form-group">
						<label for="inputEmail" class="col-lg-2 control-label">Email
							de Contacto</label>
						<div class="col-lg-10">
							<input type="text"
								class="form-control {required:true,email:true}" id="inputEmail"
								name="inputEmail" placeholder="Email del contacto del evento" />
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button class="btn btn-default">Cancelar</button>
							<button type="submit" class="btn btn-primary">Registrar</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>

	</div>
</body>
</html>