<%@page import="java.text.SimpleDateFormat"%>
<%@page import="bean.Union"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

<title>Eventos Registrados</title>
</head>
<body>
	<div class="container">


		<%
			ArrayList<Union> lista = (ArrayList<Union>) request
					.getAttribute("lista");
		%>

		<div class="bs-component">
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Se realizó con éxito el registro del evento</strong>
			</div>
		</div>
		
		<ul class="pager">
			<li class="previous"><a href="index.jsp">← Regresar</a></li>
		</ul>
		
		
		<div class="page-header">
              <h1 id="type">Eventos Registrados</h1>
            </div>

		<div class="bs-component">
			<table class="table table-striped table-hover ">
				<thead>
					<tr>
						<th>#</th>
						<th>Nombre Evento</th>
						<th>Nombre del Local</th>
						<th>Fecha Inicio</th>
						<th>Fecha Fin</th>
						<th>Capacidad</th>
						<th>URL</th>
						<th>Email Contacto</th>
					</tr>
				</thead>
				<tbody>

					<%
						SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
						SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
						for (int i = 0; i < lista.size(); i++) {
							Date di = sdf1.parse(lista.get(i).getFechaInicio());
							Date df = sdf1.parse(lista.get(i).getFechaFin());
					%>
					<tr>
						<td><%=(i + 1)%></td>
						<td><%=lista.get(i).getNombreEvento()%></td>
						<td><%=lista.get(i).getNombreLocal()%></td>
						<td><%=sdf2.format(di)%></td>
						<td><%=sdf2.format(df)%></td>
						<td><%=lista.get(i).getCapacidad()%></td>
						<td><%=lista.get(i).getUrl()%></td>
						<td><%=lista.get(i).getEmail()%></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>